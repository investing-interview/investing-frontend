# `investing-frontend`
An react.js app, design with `material-ui` lib and principles. 

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### The task ![The given Task](public/Home Assignment - Portfolio.pdf)

### How to run - instructions:
0. Make sure the `api` is ready and up (see [investing-web-api project](https://gitlab.com/investing-interview/investing-web-api))
1. clone the repo to your working space
2. `cd invensting-frontend`
3. run on the terminal `npm install package.json`
4. edit the `apiHost` variable on `utils/urls.js` (to get your api's address)
5. start the application by `npm start`

### Screens
There's only one main page, divided into 2 sections:
1. Create a new `instrument` instance in the DB.
2. Display's the current `instruments` in a grid view. (and searching on it)

## Stack:
* React.js / Hooks
* UI-lib: [material-ui](https://material-ui.com)

### Screen shots:
![The main page](public/screenshot_main-page.PNG)

### GIFs
#### Adding / removing functionality
![GIF1](public/add_delete-frontend.gif)

#### Adding entry with existing id
![GIF2](public/add_existing-frontend.gif)

#### Searching instruments functionality
![GIF3](public/search-frontend.gif)

