const apiHost = "http://localhost:4040";

export const urls = {
    getInstruments: `${apiHost}/api/instruments`,
    createInstrument: `${apiHost}/api/instruments`,
    deleteInstrument: (id) => `${apiHost}/api/instruments/${id}`
};

export const defaultSettings = {
    mode: 'cors',
    // credentials: "include", // send cookies
    cache: 'no-cache',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "localhost",
    }
};
