import React, {useState} from 'react';

import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";

import classes from './InstrumentsPanel.module.scss';

function InstrumentsPanel({data, deleteInstrument}) {

    const generateInstrumentCard = (instrument) => {
        return (
            <Card
                classes={{root: classes.card}}
                key={`instrument_card_${instrument.instrumentId}`}>
                <CardContent>
                    <h4>{instrument.name}</h4>
                    <h5>{instrument.instrumentType}</h5>
                    <h5>{instrument.symbol}</h5>
                    <h5>{instrument.instrumentId}</h5>
                </CardContent>
                <CardActions>
                    <Button size={'small'} onClick={()=>deleteInstrument(instrument.instrumentId)}>Delete</Button>
                </CardActions>
            </Card>
        )
    };

    const [searchText, setSearchText] = useState('');

    return (
        <div className={classes.wrapper}>
            <TextField classes={{root: classes.searchField}}
                       value={searchText}
                       placeholder={"Search"}
                       onChange={event => setSearchText(event.target.value)}
            />

            <div className={classes.gridWrapper}>
                {data
                    .filter(instrument=>instrument.name.startsWith(searchText))
                    .map(instrument => (generateInstrumentCard(instrument)))}
            </div>
        </div>
    );
}

export default InstrumentsPanel;
