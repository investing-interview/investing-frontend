import React from 'react'
import PropTypes from 'prop-types';

import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent/SnackbarContent";

import ErrorIcon from '@material-ui/icons/Error';
import WarningIcon from '@material-ui/icons/Warning';
import InfoIcon from '@material-ui/icons/Info';

import classes from './NotificationIndicator.module.scss';

const NotificationIndicator = ({close, isOpen, message, reason}) => {

    const iconMapping = {error: ErrorIcon, warn: WarningIcon, info: InfoIcon};
    const Icon = iconMapping[reason] || WarningIcon;

    return (
        <Snackbar
            open={isOpen}
            onClose={close}>
            <SnackbarContent
                classes={{root: classes[reason], message: classes.message}}
                message={
                    <>
                        <Icon className={classes.icon}/>
                        {message}
                    </>}
            />
        </Snackbar>);
};

NotificationIndicator.propTypes = {
    close: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    reason: PropTypes.string
};

export default NotificationIndicator;
