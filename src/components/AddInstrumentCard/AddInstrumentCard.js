import React, {useState} from 'react';

import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button"

import classes from './AddInstrumentCard.module.scss';


function AddInstrumentCard({addInstrument}) {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [symbol, setSymbol] = useState('');
    const [type, setType] = useState('');

    const loginAction = (event) => {
        event.preventDefault();
        addInstrument({id, name, symbol, type});
        setId('');
        setName('');
        setSymbol('');
        setType('');
    };
    return (
        <Paper className={classes.paperWrapper}>
            <h2 className={classes.actionTitle}>Add new instrument</h2>
            <form className={classes.addInstrumentForm} onSubmit={loginAction}>
                <TextField
                    key={"instrument_id"}
                    placeholder={'Id'}
                    value={id}
                    onChange={event => setId(event.target.value)}
                />
                <TextField
                    key={"instrument_name"}
                    placeholder={'Name'}
                    value={name}
                    onChange={event => setName(event.target.value)}
                />
                <TextField
                    key={"instrument_symbol"}
                    placeholder={'Symbol'}
                    value={symbol}
                    onChange={event => setSymbol(event.target.value)}
                />
                <TextField
                    key={"instrument_type"}
                    placeholder={'Type'}
                    value={type}
                    onChange={event => setType(event.target.value)}
                />
                <Button
                    type="submit"
                    variant="contained"
                    color={'primary'}>
                    Submit
                </Button>
            </form>
        </Paper>
    );
}

export default AddInstrumentCard;
