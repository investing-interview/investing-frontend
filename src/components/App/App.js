import React, {useEffect, useState} from 'react';

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import AddInstrumentCard from "../AddInstrumentCard";
import InstrumentsPanel from "../InstumentsPanel";
import NotificationIndicator from "../NotificationIndicator";

import {defaultSettings, urls} from "../../utils/urls";
import classes from './App.module.scss';

function App() {

    const [instruments, setInstruments] = useState([]);

    const addInstrument = (instrument) => {
        fetch(urls.createInstrument, {method: 'POST', body: JSON.stringify(instrument), ...defaultSettings})
            .then(resp => {
                return resp.json()
            }).then(({success, data, msg}) => {
                if (success) {
                    // Add to store
                    setInstruments([...instruments, data]);
                    showNotification(msg, 'info');
                }
                else{
                    showNotification(msg, 'error')
                }
            }
        )
            .catch(err => console.error("failed to add new instrument", err)
            );
    };
    const deleteInstrument = (id) => {
        fetch(urls.deleteInstrument(id), {method: 'DELETE', ...defaultSettings})
            .then(resp => {
                return resp.json()
            }).then(({success, msg}) => {
                if (success) {
                    // Add to store
                    setInstruments(instruments.filter(inst=> inst.instrumentId && inst.instrumentId !== id));
                    showNotification(msg, 'info');
                }
                else{
                    showNotification(msg, 'warn');
                }
            }
        )
            .catch(err => {
                    console.error("failed to delete instrument", err);
                    showNotification("failed to delete instrument", 'error')
                }
            );
    };

    // Load data at the initial loading
    useEffect(() => {
        fetch(urls.getInstruments, {method: 'GET', ...defaultSettings})
            .then(resp => {
                if (resp.status === 200) {
                    return resp.json()
                }
            }).then(({data}) => {
                setInstruments(data)
            }
        ).catch(err => {
            console.error("failed to fetch instruments", err);
            showNotification("failed to fetch instruments", 'error')

        })
    }, []);

    //Notification state
    const [isNotificationOpen, setNotificationOpen] = useState(false);
    const [notificationMessage, setNotificationMessage] = useState('');
    const [notificationReason, setNotificationReason] = useState('info');
    const showNotification = (message, reason) => {
        setNotificationMessage(message);
        setNotificationReason(reason);
        setNotificationOpen(true);
    };

    return (
        <>
            <AppBar classes={{root: classes.AppBarRoot}}>
                <Toolbar>
                    <h3>Investing Home Assignment</h3>
                </Toolbar>
            </AppBar>

            <div className={classes.pageWrapper}>
                <AddInstrumentCard addInstrument={addInstrument}/>
                <InstrumentsPanel data={instruments} deleteInstrument={deleteInstrument}/>
            </div>

            <NotificationIndicator
                isOpen={isNotificationOpen}
                message={notificationMessage}
                reason={notificationReason}
                close={() => setNotificationOpen(false)}
            />

        </>
    );
}

export default App;
